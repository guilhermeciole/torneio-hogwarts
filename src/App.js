import "./App.css";
import Estudantes from "./components/Estudantes";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1 className="titulo">Torneio Hogwarts</h1>
        <Estudantes />
      </header>
    </div>
  );
}

export default App;
