import { useState } from "react";
import "./style.css";
import Cartas from "../Cartas";

const AleatoriosEstudantes = ({ listaDeBruxos }) => {
  const [trioDeBruxos, setTrioDeBruxos] = useState([]);

  const aleatorio1 = Math.floor(Math.random() * listaDeBruxos.length);

  const distinto2 = listaDeBruxos.filter((item) => {
    return item.house !== listaDeBruxos[aleatorio1].house;
  });
  const aleatorio2 = Math.floor(Math.random() * distinto2.length);

  const distinto3 = distinto2.filter((item) => {
    return item.house !== distinto2[aleatorio2].house;
  });
  const aleatorio3 = Math.floor(Math.random() * distinto3.length);

  const bruxos = () => {
    setTrioDeBruxos([
      listaDeBruxos[aleatorio1],
      distinto2[aleatorio2],
      distinto3[aleatorio3],
    ]);
  };

  return (
    <div>
      <Cartas trioDeBruxos={trioDeBruxos} />
      <button onClick={bruxos}>Gerar bruxos</button>
    </div>
  );
};

export default AleatoriosEstudantes;
