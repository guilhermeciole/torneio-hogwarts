import { useEffect, useState } from "react";
import AleatoriosEstudantes from "../AleatoriosEstudantes";

const Estudantes = () => {
  const [listaDeBruxos, setListaDeBruxos] = useState([]);

  useEffect(() => {
    fetch("https://hp-api.herokuapp.com/api/characters/students")
      .then((response) => response.json())
      .then((response) => setListaDeBruxos(response));
  }, []);

  return <AleatoriosEstudantes listaDeBruxos={listaDeBruxos} />;
};

export default Estudantes;
