import "./style.css"

function Cartas({ trioDeBruxos }) {
  return (
    <div className="container">
      {trioDeBruxos.map((bruxo) => {
        return (
          <div className="bruxo">
            <img src={bruxo.image}></img>
            <h1>{bruxo.name}</h1>
            <p>{bruxo.house}</p>
          </div>
        );
      })}
    </div>
  );
}

export default Cartas;
